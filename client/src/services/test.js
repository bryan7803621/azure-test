import axios from 'axios'
const baseUrl = 'https://ambitious-bay-06269120f.5.azurestaticapps.net/api/test'

const getTestJson = () => {
  const request = axios.get(baseUrl)
  return request.then(response => response.data)
}

export default { getTestJson }