import React from 'react';
import { useEffect, useState } from 'react';
import testService from './services/test'

function App() {
  const [value, setValue] = useState("World")

  useEffect(() => {
    setValue("Bryan")
    const example = testService.getTestJson()
    console.log(example);
  }, [])

  return <div>Hello {value}</div>;
}

export default App;
