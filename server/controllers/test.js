const { default: test } = require('node:test')

const testRouter = require('express').Router()

testRouter.get('/', (request, response) => {
  const testJson = {
    name: "Bryan",
    age: 37,
}
  response.json(testJson)
})

module.exports = testRouter



