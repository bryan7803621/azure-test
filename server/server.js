const express = require('express')
const port = process.env.PORT || 3001;
const testRouter = require('./controllers/test')

const app = express()

app.use(express.json())
app.use(express.static('./client/build'))

app.get("*", (request, response) => {
  response.sendFile(path.resolve(__dirname, "client", "build", "index.html"))
})

app.use('/api/test', testRouter)

app.listen(port, () => {
  console.log(`Server is running on post ${port}`);
});